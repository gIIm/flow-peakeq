import("stdfaust.lib");

nBands = 4;
eq(N) = hgroup("Parametric EQ",seq(i,N,peakeq(i)))
with{
  peakeq(i) = vgroup("[%i]Band %index",fi.peak_eq(l,f,b))
  with{
	index = i+1;
	freq = 1000 * index;
    l = vslider("[0]Level[unit:db]",0,-70,12,0.01) : si.smoo;
    f = nentry("[1]Peak Frequency[unit:Hz]",freq,20,20000,1) : si.smoo;
    b = f/hslider("[2]Bandwidth[style:knob][unit:Hz]",1,1,50,0.01) : si.smoo;
  };
};
process = _ <: eq(nBands);
