/*
  ==============================================================================

    CustomLookAndFeel.h
    Created: 26 Apr 2019 3:44:41am
    Author:  gllm

  ==============================================================================
*/

#pragma once

#include "../JuceLibraryCode/JuceHeader.h"

//==============================================================================
class LookAndFeel_V4_DocumentWindowButton : public Button
{
public:
	LookAndFeel_V4_DocumentWindowButton(const String& name, Colour c, const Path& normal, const Path& toggled)
		: Button(name), colour(c), normalShape(normal), toggledShape(toggled)
	{
	}

	void paintButton(Graphics& g, bool shouldDrawButtonAsHighlighted, bool shouldDrawButtonAsDown) override
	{
		auto background = Colours::grey;

		if (auto* rw = findParentComponentOfClass<ResizableWindow>())
			if (auto lf = dynamic_cast<LookAndFeel_V4*> (&rw->getLookAndFeel()))
				background = Colour(36, 36, 36).darker(0.4f);

		g.fillAll(background);

		g.setColour((!isEnabled() || shouldDrawButtonAsDown) ? colour.withAlpha(0.6f)
			: colour);

		if (shouldDrawButtonAsHighlighted)
		{
			g.fillAll();
			g.setColour(background);
		}

		auto& p = getToggleState() ? toggledShape : normalShape;

		auto reducedRect = Justification(Justification::centred)
			.appliedToRectangle(Rectangle<int>(getHeight(), getHeight()), getLocalBounds())
			.toFloat()
			.reduced(getHeight() * 0.3f);

		g.fillPath(p, p.getTransformToScaleToFit(reducedRect, true));
	}

private:
	Colour colour;
	Path normalShape, toggledShape;

	JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR(LookAndFeel_V4_DocumentWindowButton)
};

//==============================================================================
class CustomLookAndFeel : public LookAndFeel_V4
{
public:
	CustomLookAndFeel()
	{

		setColour(DocumentWindow::textColourId, white);
		
		setColour(ResizableWindow::backgroundColourId, charcoal);

		setColour(TextButton::buttonColourId, lightGrey.brighter());
		setColour(TextButton::buttonOnColourId, lightGrey);
		setColour(TextButton::textColourOnId, white.darker(0.1f));
		setColour(TextButton::textColourOffId, white);

		setColour(ToggleButton::textColourId, white);
		setColour(ToggleButton::tickColourId, white);
		setColour(ToggleButton::tickDisabledColourId, lightGrey.darker(0.2f));

		setColour(Label::backgroundColourId, charcoal);
		setColour(Label::textColourId, white);
		setColour(Label::outlineColourId, lightGrey);
		setColour(Label::textWhenEditingColourId, white.brighter(0.1f));

		setColour(Slider::backgroundColourId, lightGrey.darker(0.6f));
		setColour(Slider::thumbColourId, white.brighter(0.1f));
		setColour(Slider::trackColourId, lightGrey.brighter(0.2f));
		setColour(Slider::rotarySliderFillColourId, white.brighter(0.1f));
		setColour(Slider::rotarySliderOutlineColourId, lightGrey);
		setColour(Slider::textBoxTextColourId, white);
		setColour(Slider::textBoxBackgroundColourId, charcoal);
		setColour(Slider::textBoxHighlightColourId, white.brighter(0.1f));
		setColour(Slider::textBoxOutlineColourId, lightGrey);

		setColour(PopupMenu::backgroundColourId, charcoal);
		setColour(PopupMenu::textColourId, white);
		setColour(PopupMenu::headerTextColourId, white.darker());
		setColour(PopupMenu::highlightedTextColourId, white.brighter());
		setColour(PopupMenu::highlightedBackgroundColourId, charcoal.brighter());

		setColour(ComboBox::buttonColourId, charcoal);
		setColour(ComboBox::outlineColourId, white);
		setColour(ComboBox::textColourId, white);
		setColour(ComboBox::backgroundColourId, charcoal);
		setColour(ComboBox::arrowColourId, white);
		setColour(ComboBox::focusedOutlineColourId, charcoal.brighter());

		setColour(ListBox::backgroundColourId, charcoal);
		setColour(ListBox::outlineColourId, white);
		setColour(ListBox::textColourId, white);

		setColour(ScrollBar::backgroundColourId, charcoal.darker());
		setColour(ScrollBar::thumbColourId, lightGrey);
		setColour(ScrollBar::trackColourId, lightGrey.brighter(0.1f));

		setColour(AlertWindow::backgroundColourId, charcoal);
		setColour(AlertWindow::textColourId, white);
		setColour(AlertWindow::outlineColourId, lightGrey);

	}

	Typeface::Ptr getTypefaceForFont(const Font& f) override
	{
		Font newFont = f;

		//const char* newAlphabet = BinaryData::new_alphabet_regular_ttf;
		//const int newAlphabetSize = BinaryData::new_alphabet_regular_ttfSize;

		//const char* gridnik = BinaryData::gridnik_ttf;
		//const int gridnikSize = BinaryData::gridnik_ttfSize;

		//const char* jura = BinaryData::jura_regular_ttf;
		//const int juraSize = BinaryData::jura_regular_ttfSize;

		//const char* orbitron = BinaryData::orbitron_regular_ttf;
		//const int orbitronSize = BinaryData::orbitron_regular_ttfSize;

		const char* shareTechMono = BinaryData::share_tech_mono_regular_ttf;
		const int shareTechMonoSize = BinaryData::share_tech_mono_regular_ttfSize;

		//const char* averageMono = BinaryData::average_mono_ttf;
		//const int averageMonoSize = BinaryData::average_mono_ttfSize;

		static Typeface::Ptr myFont = Typeface::createSystemTypefaceFor(shareTechMono,
			shareTechMonoSize);

		return myFont;
	}

	Font getPopupMenuFont() override
	{
		return Font(16.0f);
	}

	virtual int getDefaultMenuBarHeight() override
	{
		return 22;
	}

	void drawDocumentWindowTitleBar(DocumentWindow& window, Graphics& g,
		int w, int h, int titleSpaceX, int titleSpaceW,
		const Image* icon, bool drawTitleTextOnLeft) override
	{
		if (w * h == 0)
			return;

		auto isActive = window.isActiveWindow();

		g.setColour(charcoal.darker(0.4f));
		g.fillAll();

		Font font(h * 0.65f, Font::plain);
		g.setFont(font);

		auto textW = font.getStringWidth(window.getName());
		auto iconW = 0;
		auto iconH = 0;

		if (icon != nullptr)
		{
			iconH = static_cast<int> (font.getHeight());
			iconW = icon->getWidth() * iconH / icon->getHeight() + 4;
		}

		textW = jmin(titleSpaceW, textW + iconW);
		auto textX = drawTitleTextOnLeft ? titleSpaceX
			: jmax(titleSpaceX, (w - textW) / 2);

		if (textX + textW > titleSpaceX + titleSpaceW)
			textX = titleSpaceX + titleSpaceW - textW;

		if (icon != nullptr)
		{
			g.setOpacity(isActive ? 1.0f : 0.6f);
			g.drawImageWithin(*icon, textX, (h - iconH) / 2, iconW, iconH,
				RectanglePlacement::centred, false);
			textX += iconW;
			textW -= iconW;
		}

		if (window.isColourSpecified(DocumentWindow::textColourId) || isColourSpecified(DocumentWindow::textColourId))
			g.setColour(window.findColour(DocumentWindow::textColourId));
		else
			g.setColour(getCurrentColourScheme().getUIColour(ColourScheme::defaultText));

		g.drawText(window.getName(), textX, 0, textW, h, Justification::centredLeft, true);
	}

	Button* createDocumentWindowButton(int buttonType) override
	{
		Path shape;
		auto crossThickness = 0.15f;

		if (buttonType == DocumentWindow::closeButton)
		{
			shape.addLineSegment({ 0.0f, 0.0f, 1.0f, 1.0f }, crossThickness);
			shape.addLineSegment({ 1.0f, 0.0f, 0.0f, 1.0f }, crossThickness);

			return new LookAndFeel_V4_DocumentWindowButton("close", Colour(white), shape, shape);
		}

		if (buttonType == DocumentWindow::minimiseButton)
		{
			shape.addLineSegment({ 0.0f, 0.5f, 1.0f, 0.5f }, crossThickness);

			return new LookAndFeel_V4_DocumentWindowButton("minimise", Colour(white), shape, shape);
		}

		if (buttonType == DocumentWindow::maximiseButton)
		{
			shape.addLineSegment({ 0.5f, 0.0f, 0.5f, 1.0f }, crossThickness);
			shape.addLineSegment({ 0.0f, 0.5f, 1.0f, 0.5f }, crossThickness);

			Path fullscreenShape;
			fullscreenShape.startNewSubPath(45.0f, 100.0f);
			fullscreenShape.lineTo(0.0f, 100.0f);
			fullscreenShape.lineTo(0.0f, 0.0f);
			fullscreenShape.lineTo(100.0f, 0.0f);
			fullscreenShape.lineTo(100.0f, 45.0f);
			fullscreenShape.addRectangle(45.0f, 45.0f, 100.0f, 100.0f);
			PathStrokeType(30.0f).createStrokedPath(fullscreenShape, fullscreenShape);

			return new LookAndFeel_V4_DocumentWindowButton("maximise", Colour(white), shape, fullscreenShape);
		}

		jassertfalse;
		return nullptr;
	}

	Colour white = Colour(255, 255, 255);
	Colour darkGrey = Colour(28, 28, 28);
	Colour lightGrey = Colour(146, 146, 146);
	Colour dimGray = Colour(48, 48, 48);
	Colour charcoal = Colour(36, 36, 36);
	Colour lightGreen = Colour(127, 255, 0);
	Colour lightBlue = Colour(0, 246, 255);
	Colour lightPink = Colour(255, 185, 185);

private:

	JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR(CustomLookAndFeel)
};